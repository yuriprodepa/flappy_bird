package com.game.android;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class MyFlappyBird extends ApplicationAdapter {

	//private int movimentoX = 0;
	//private int velocidade = 5;
	private float variacaoAsa = 0;
	private float gravidade = 0;
	private float posInicVerticalPassaro = 0; // Posição inicial do passaro
	private SpriteBatch batch; // classe para exibir imagens e formas dentro do jogo

	//Texturas
	private Texture[] passaros;
	private Texture fundo;
	private Texture[] canoBaixo;
	private Texture[] canoTopo;
	private Texture gameOver;

	//Formas para colisão
	private ShapeRenderer shapeRenderer; // para visualizar as formas
	private Circle circuloPassaro;
	private Rectangle retanguloCanoCima, retanguloCanoBaixo;


	// Parametros
	private float larguraDisplay;
	private float alturaDisplay;
	private int espacoEntreCanos;
	private float posicaoCanoHorizontal; /// variável que posiciona o cano horizontalmente
	private float posicaoCanoVertical; // para variar posição da abertura entre os canos
	private Random random;
	private boolean debuging = false;
	private int estadoJogo = 0;

	//Parametros colunas
	float yCima,yBaixo,variacaoCima,variacaoBaixo;

	// Exibição de texto
	private BitmapFont textoPontuacao, textoReiniciar, textoMelhorPontuacao;

	private int pontuacao = 0;
	private int pontuacaoRecord = 0;
	private boolean passouCano = false;

	//Configurar Sons
	Sound somVoando;
	Sound somColisao;
	Sound somPontuacao;

	@Override
	public void create () {
		//Gdx.app.log("Gdx", "Jogo Iniciado");
		inicializarTexturas();
		inicializarObjetos();
	}

	@Override
	public void render () {

		verificaEstado();
		validarPontos();
		desenharTexturas();
		detectarColisoes();

	}

	/*
		0 - Jogo iniciado, passaro parado
		1 - Começa o jogo
		3 - Colidiu
	*/
	private void verificaEstado(){
		boolean toqueTela = Gdx.input.justTouched();
		int alturasalto = -40;

		//Bate assas
		if(variacaoAsa > 2) variacaoAsa = 0;
		variacaoAsa += Gdx.graphics.getDeltaTime() * 8;

		if(estadoJogo == 0){
			if(toqueTela){
				gravidade = alturasalto;
				estadoJogo = 1;
				somVoando.play();
			}
		}else if(estadoJogo == 1){
			//Movimenta o cano
			posicaoCanoHorizontal -= Gdx.graphics.getDeltaTime() * 200;
			if(posicaoCanoHorizontal < - canoBaixo[0].getWidth()){
				posicaoCanoHorizontal = larguraDisplay;
				posicaoCanoVertical = random.nextInt(espacoEntreCanos) - (espacoEntreCanos/2);
			}

			//Aplica evento de click na tela
			if(toqueTela){
				gravidade = alturasalto;
				somVoando.play();
			}

			//Aplica gravidade do passaro
			if(posInicVerticalPassaro > 0 || gravidade < 0)
				posInicVerticalPassaro = posInicVerticalPassaro - (gravidade * (Gdx.graphics.getDeltaTime() * 8));
			gravidade++;


		}else if(estadoJogo == 2){
			if(toqueTela){
				pontuacao = 0;
				gravidade = 0;
				posInicVerticalPassaro = alturaDisplay /2;
				posicaoCanoHorizontal = larguraDisplay;
				estadoJogo = 0;
			}
		}

	}

	private void detectarColisoes(){

		circuloPassaro.set(50 + (passaros[0].getWidth() / 2), posInicVerticalPassaro + (passaros[0].getHeight() / 2), (passaros[0].getHeight() / 2));
		retanguloCanoCima.set(posicaoCanoHorizontal, yCima + variacaoCima, canoTopo[1].getWidth(), canoTopo[1].getHeight());
		retanguloCanoBaixo.set(posicaoCanoHorizontal, yBaixo + variacaoBaixo, canoBaixo[1].getWidth(), canoBaixo[1].getHeight());

		// Verifica colisão
		if((Intersector.overlaps(circuloPassaro,retanguloCanoCima))||
		   (Intersector.overlaps(circuloPassaro,retanguloCanoBaixo))){
			if(estadoJogo == 1) {
				estadoJogo = 2;
				somColisao.play();
				Gdx.app.log("Log", "Colidiu com o cano.");
			}
		}

		if(debuging) {
			shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
			shapeRenderer.circle(50 + (passaros[0].getWidth() / 2), posInicVerticalPassaro + (passaros[0].getHeight() / 2), (passaros[0].getWidth() / 2));
			shapeRenderer.rect(posicaoCanoHorizontal, yCima + variacaoCima, canoTopo[1].getWidth(), canoTopo[1].getHeight());
			shapeRenderer.rect(posicaoCanoHorizontal, yBaixo + variacaoBaixo, canoBaixo[1].getWidth(), canoBaixo[1].getHeight());
			shapeRenderer.end();
		}

	}

	private void inicializarTexturas(){
		passaros = new Texture[3];
		passaros[0] = new Texture("passaro1.png");
		passaros[1] = new Texture("passaro2.png");
		passaros[2] = new Texture("passaro3.png");
		fundo = new Texture("fundo.png");
		canoTopo = new Texture[2];
		canoTopo[0] = new Texture("cano_topo.png");
		canoTopo[1] = new Texture("cano_topo_maior.png");
		canoBaixo = new Texture[2];
		canoBaixo[0] = new Texture("cano_baixo.png");
		canoBaixo[1] = new Texture("cano_baixo_maior.png");

		gameOver = new Texture("game_over.png");

	}

	private void validarPontos(){
		if( posicaoCanoHorizontal < 50 - passaros[0].getWidth()){ // Passou da posição do passaro
			if(!passouCano) {
				passouCano = true;
				pontuacao++;
				somPontuacao.play();
			}
		}else{
			passouCano = false;
		}
	}

	private void desenharTexturas(){
		batch.begin();
		batch.draw(fundo,0,0,larguraDisplay,alturaDisplay);
		batch.draw(passaros[(int) variacaoAsa],50,posInicVerticalPassaro);
		desenhaObstaculos();
		desenhaTextos();
		if(estadoJogo == 2){
			batch.draw(gameOver,
					larguraDisplay/2-(gameOver.getWidth()/2),
					alturaDisplay - 400);
			if(pontuacao > pontuacaoRecord) pontuacaoRecord = pontuacao;

			textoMelhorPontuacao.draw(batch, "Seu recorde é: "+pontuacaoRecord+" pontos",
					larguraDisplay/2 - 300,
					alturaDisplay - 420);

			textoReiniciar.draw(batch, "Toque na tela \n para reiniciar",
					larguraDisplay/2 - 210,
					alturaDisplay - 510);
		}
		batch.end();
	}

	private void desenhaTextos(){
		textoPontuacao.draw(batch, String.valueOf(pontuacao), (larguraDisplay/2) - (textoPontuacao.getScaleY() + 10), alturaDisplay - 110);

	}

	//+ ((espacoEntreCanos)/2 - posicaoCanoVertical)
	private void desenhaObstaculos(){
		yCima = (alturaDisplay - canoTopo[1].getHeight());
		variacaoCima = ((espacoEntreCanos/2) - posicaoCanoVertical);
		yBaixo = 0 - (espacoEntreCanos);
		variacaoBaixo = (espacoEntreCanos/2) - posicaoCanoVertical;

		batch.draw(canoTopo[1], posicaoCanoHorizontal,yCima + variacaoCima);
		batch.draw(canoBaixo[1],posicaoCanoHorizontal, yBaixo + variacaoBaixo);

	}

	private void inicializarObjetos(){
		batch = new SpriteBatch();
		random = new Random();

		larguraDisplay = Gdx.graphics.getWidth();
		alturaDisplay = Gdx.graphics.getHeight();
		posInicVerticalPassaro = alturaDisplay /2;
		posicaoCanoHorizontal = larguraDisplay;
		espacoEntreCanos = 400;

		//Configuracoes dos textos
		textoPontuacao = new BitmapFont();
		textoPontuacao.setColor(Color.WHITE);
		textoPontuacao.getData().setScale(10);

		textoReiniciar = new BitmapFont();
		textoReiniciar.setColor(Color.WHITE);
		textoReiniciar.getData().setScale(5);

		textoMelhorPontuacao = new BitmapFont();
		textoMelhorPontuacao.setColor(Color.RED);
		textoMelhorPontuacao.getData().setScale(4);

		//Formas Geométricas para colisoes
		circuloPassaro = new Circle();
		retanguloCanoBaixo = new Rectangle();
		retanguloCanoCima = new Rectangle();
		shapeRenderer = new ShapeRenderer();

		//Inicializa os sons
		somVoando = Gdx.audio.newSound(Gdx.files.internal("som_asa.wav"));
		somColisao = Gdx.audio.newSound(Gdx.files.internal("som_batida.wav"));
		somPontuacao = Gdx.audio.newSound(Gdx.files.internal("som_pontos.wav"));
	}

	@Override
	public void dispose () {
		Gdx.app.log("Gdx", "Descarte de conteudo");
	}
}
